
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import {  ImagesService } from '../images.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {
  title:string;
  Category:string;
  id:string;
  userId:string;
  category:string = "Loading..";
  categoryImage:string;
  categories:object[] = [{id:1,cat: 'business'}, {id:2,cat: 'entertainment'}, {id:3,cat: 'politics'},{id:4,cat: 'sport'}, {id:5,cat: 'tech'}];

  constructor(public classifyService:ClassifyService,private router: Router,private route:ActivatedRoute, 
    public imagesService:ImagesService, public Authservice:AuthService ) { }
  
    onSubmit() {
    this.classifyService.addClassify(this.userId,this.title,this.Category);
   this.router.navigate(['/saveclassify']);
    }
  ngOnInit() {
  
  this.classifyService.classify().subscribe(
    res => {
      this.category = this.classifyService.categories[res];
      console.log(this.category);
      this.categoryImage = this.imagesService.images[res];
    } 
  )
  
  }

}
