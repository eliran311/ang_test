import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
public doc:string;
userCollection:AngularFirestoreCollection = this.db.collection('users');
classifyCollection:AngularFirestoreCollection;

getClassify(userId:string):Observable<any[]>{
  // return this.db.collection('books').valueChanges({idField:'id'});
this.classifyCollection = this.db.collection(`users/${userId}/classify`)
return this.classifyCollection.snapshotChanges().pipe(
  map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data; 
      }


    )
  )
)
}

addClassify(userId:string,  title:string,category:string){
  const article = {title:title, category:category};
// this.db.collection('books').add(book)
this.userCollection.doc(userId).collection('classify').add(article);

}
getclassify(id:string, userId:string):Observable<any>{
  return this.db.doc(`users/${userId}/classify/${id}`).get();
}
classify():Observable<any>{
   let json = {
     "articles":[
       {"text":this.doc} 
     ]
   }
let body = JSON.stringify(json); //סטרינג יודע לעבוד בhttp ולכן עושים שיטוח לdhhxui
return this.http.post<any>(this.url,body).pipe(
  map(res =>{
    let final = res.body.replace('[','');
    final = final.replace(']','');
    return final;
  })
)

}



constructor(private http:HttpClient,private db:AngularFirestore) { }
}
