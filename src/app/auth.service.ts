import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
 import { User } from './interfaces/user';
 import { Router, ActivatedRoute } from '@angular/router';
 
@Injectable({
  providedIn: 'root'
})
export class AuthService {
 user: Observable<User| null>
 text:boolean = false;

 constructor(public afAuth:AngularFireAuth,private router: Router,private route:ActivatedRoute) { 
   this.user = this.afAuth.authState;
}


getUser(){
  return this.user 
}




signup(email:string, password:string){
     this.afAuth
     .auth.createUserWithEmailAndPassword(email,password).then
      (res => { console.log('Succesful Signup', res);
       this.router.navigate(['/success']);
      }
       )
     .catch(function(error) {
      // Handle Errors here.
      // var errorCode = error.code;
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(errorMessage);
      console.log(error);
      console.log(errorCode);
      this.router.navigate(['/login']);
    });
  
      
    
  
    //  .then
    //  (res => { console.log('Succesful Signup', res);
    //  this.router.navigate(['/login']);
    // }
    //  )
  }
    signin(email:string, password:string){
      this.afAuth
      .auth.signInWithEmailAndPassword(email,password).then
      (res => { console.log('Succesful login', res);
       this.router.navigate(['/success']);
      }
       )
      .catch(function(error) {
        // Handle Errors here.
        // var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorMessage);
        console.log(error);
        this.router.navigate(['/login']);
      });
      // .then(
        // res => { console.log('Succesful login', res);
    //  }
      // )
    }
    logout(){
      this.afAuth.auth.signOut();
      this.router.navigate(['/login']);
     }
}