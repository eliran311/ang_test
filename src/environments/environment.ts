// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAhWUcbmUSGY2B-sA11Q-1731-mUJFLhUs",
    authDomain: "test-ang-273c5.firebaseapp.com",
    databaseURL: "https://test-ang-273c5.firebaseio.com",
    projectId: "test-ang-273c5",
    storageBucket: "test-ang-273c5.appspot.com",
    messagingSenderId: "268926597226",
    appId: "1:268926597226:web:1eb5e5ce91246963f07213",
    measurementId: "G-V3MJ37V1JQ"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
